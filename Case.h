#pragma once
#ifndef CASE_H
#define CASE_H

#include <string>

class Case
{
public:
	Case();
	virtual ~Case();

	int getNumber();
	bool isBomb() const;
	bool isHidden() const;
	bool isFlaged() const;

	void reset();
	void setDiscovered();
	void setHidden();
	void setNumber(int n);
	void setBomb(bool here);
	void setFlag();
	void unsetFlag();
	void incNumber();
	bool isSelected();
	void Select();
	void Unselect();

private:
	bool flag;
	bool discovered;
	bool bomb;
	int number;
	bool selected;
};


#endif // CASE_H