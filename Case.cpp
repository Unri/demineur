#include "stdafx.h"
#include "Case.h"


Case::Case()
{
	flag = false;
	discovered = false;
	bomb = false;
	selected = false;
	number = 0;
}


Case::~Case()
{
}


/**
  * @brief renvoie le contenu de la case
  *	@return -1 si c'est une bombe, sinon le num�ro
  */
int Case::getNumber() {
	return number;
}

bool Case::isBomb() const{
	return bomb;
}

bool Case::isHidden() const{
	return !discovered;
}

bool Case::isFlaged() const{
	return flag;
}

void Case::setDiscovered() {
	discovered = true;
}

void Case::setNumber(int n) {
	number = n;
}

void Case::setBomb(bool here) {
	bomb = here;
}

void Case::setHidden() {
	discovered = false;
}

void Case::setFlag() {
	flag = true;
}

void Case::unsetFlag() {
	flag = false;
}

void Case::incNumber() {
	number++;
}

bool Case::isSelected() {
	return selected;
}

void Case::Select() {
	selected = true;
}

void Case::Unselect() {
	selected = false;
}

void Case::reset() {
	flag = false;
	discovered = false;
	bomb = false;
	selected = false;
	number = 0;
}