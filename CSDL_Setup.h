#pragma once
#ifndef CSDL_SETUP_H
#define CSDL_SETUP_H


#include <iostream>
#include <SDL.h>
#include <SDL/SDL_image.h>


class CSDL_Setup
{
public:
	CSDL_Setup(std::string windowName,int ScreenWidth, int ScreenHeight);
	~CSDL_Setup();

	SDL_Renderer* getRenderer();
	SDL_Event* getMainEvent();
	SDL_WindowEvent* getWindowEvent();

	void Begin();
	void End();

	int getScreenWidth();
	int getScreenHeight();
	void MinimizeWindow();

private:

	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Event *MainEvent;
	SDL_WindowEvent *WindowEvent;

	int ScreenWidth;
	int ScreenHeight;
	void RestoreWindow();
};

#endif // !CSDL_SETUP_H