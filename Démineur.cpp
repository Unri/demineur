// Démineur.cpp : Définit le point d'entrée pour l'application console.
//

#include "stdafx.h"
#include "CSDL_Setup.h"
#include "GameScreen.h"
#include "const.h"



int main(int argc, char *args[])
{
	int tabWidth = 14, tabHeight = 11;
	// 30 16
	GameScreen* game = new GameScreen("Démineur", tabWidth, tabHeight);

	game->LoopScreen();

	delete game;

    return 0;
}

