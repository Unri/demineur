#include "stdafx.h"
#include "CSDL_Setup.h"
#include <SDL\SDL.h>
#include <SDL_main.h>
#include<SDL\SDL_image.h>


CSDL_Setup::CSDL_Setup(std::string windowName, int passed_ScreenWidth, int passed_ScreenHeight)
{
	ScreenWidth = passed_ScreenWidth;
	ScreenHeight = passed_ScreenHeight;

	window = NULL;
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) { // Initialisation de la SDL	
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}
	IMG_Init(IMG_INIT_PNG);
	
	// Ouverture de la fen�tre
	window = SDL_CreateWindow(windowName.c_str(), 0, 0, ScreenWidth, ScreenHeight, SDL_WINDOW_BORDERLESS);
	if (window == NULL) // Si l'ouverture a �chou�, on le note et on arr�te
	{
		fprintf(stderr, "Impossible de charger le mode vid�o : %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}
	renderer = NULL;
	renderer = SDL_CreateRenderer(window, -1, 0);
	if (renderer == NULL) // Si l'ouverture a �chou�, on le note et on arr�te
	{
		fprintf(stderr, "Impossible de charger le render : %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	SDL_SetRenderDrawColor(renderer, 192, 192, 192, 255);

	MainEvent = new SDL_Event();
}

CSDL_Setup::~CSDL_Setup()
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	delete MainEvent;
	MainEvent = NULL;

	IMG_Quit();
	SDL_Quit(); // Arr�t de la SDL
}

SDL_Renderer* CSDL_Setup::getRenderer() {
	return renderer;
}

SDL_Event* CSDL_Setup::getMainEvent() {
	MainEvent->button.clicks = 1;
	return MainEvent;
}

SDL_WindowEvent* CSDL_Setup::getWindowEvent() {
	return WindowEvent;
}

void CSDL_Setup::Begin() {
	SDL_PollEvent(MainEvent);
	SDL_RenderClear(renderer);
}

void CSDL_Setup::End() {
	SDL_RenderPresent(renderer);
}

void CSDL_Setup::MinimizeWindow() {
	SDL_MinimizeWindow(window);
}

void CSDL_Setup::RestoreWindow() {
	SDL_RestoreWindow(window);
}

int CSDL_Setup::getScreenWidth() {
	return ScreenWidth;
}

int CSDL_Setup::getScreenHeight() {
	return ScreenHeight;
}