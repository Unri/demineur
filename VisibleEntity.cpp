#include "stdafx.h"
#include "const.h"
#include "VisibleEntity.h"


VisibleEntity::VisibleEntity(SDL_Renderer* passed_renderer, SDL_Texture* passed_texture, int width, int height, int y)
{
	renderer = passed_renderer;
	texture = passed_texture;

	pos.w = width*PRP;
	pos.h = height*PRP;

	rect.w = width;
	rect.h = height;
	rect.y = y;
	rect.x = BORD;
}

VisibleEntity::~VisibleEntity()
{
}

void VisibleEntity::Draw() {
	SDL_RenderCopy(renderer, texture, &rect, &pos);
}

void VisibleEntity::setPos(int x, int y) {
	pos.x = x*PRP;
	pos.y = y*PRP;
}

void VisibleEntity::setRect(int x, int y) {
	rect.x = x;
	rect.y = y;
}

bool VisibleEntity::ClickedOn(int x, int y) {
	return (x >= pos.x && x <= pos.x + pos.w &&
		y >= pos.y && y <= pos.y + pos.h);
}

void VisibleEntity::DrawCount(int PosXUniteR, int value, int nbChiffre) {
	unsigned int Puiss10 = 1;
	for (int i = 0; i < nbChiffre; ++i) {
		setPos((PosXUniteR-i)*CASE_SIZE, CASE_SIZE + 1);
		setRect(BORD + (COUNT_W + 1)*(value / Puiss10 % 10), Y_COUNT);
		Draw();
		Puiss10 *= 10;
	}
}