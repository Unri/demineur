#pragma once
#ifndef CONST_H
#define CONST_H


#include "stdafx.h"

#define PRP 2
#define CASE_SIZE 16
#define COUNT_H 23
#define COUNT_W 13
#define SMILEY_SIZE 26
#define Y_COUNT 2
#define Y_SMILEY 26
#define Y_CASE 53
#define Y_NUMBER 70
#define BORD 2

#endif // !CONST_H