#pragma once
#ifndef VISIBLE_ENTITY_H
#define VISIBLE_ENTITY_H

#include <SDL\SDL.h>
#include <SDL\SDL_image.h>

class VisibleEntity
{
public:
	VisibleEntity(SDL_Renderer* passed_renderer, SDL_Texture* passed_texture, int width, int height, int y);
	virtual ~VisibleEntity();

	void Draw();
	void setPos(int x, int y);
	void setRect(int x, int y);
	bool ClickedOn(int x, int y);
	void DrawCount(int tabWidth, int timer, int nbChiffre);

private:
	SDL_Rect pos;
	SDL_Rect rect;

	SDL_Renderer* renderer;
	SDL_Texture* texture;
};

#endif VISIBLE_ENTITY_H