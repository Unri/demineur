#include "stdafx.h"
#include <iostream>
#include <time.h>
#include <assert.h>
#include "CSDL_Setup.h"
#include "GameScreen.h"

GameScreen::GameScreen(std::string windowName, int passed_tabWidth, int passed_tabHeight)
{

	repeat = false;
	quit = false;
	setup = new CSDL_Setup(windowName, (passed_tabWidth + 2)*CASE_SIZE*PRP, ((passed_tabHeight + 3)*CASE_SIZE + SMILEY_SIZE)*PRP);
	spite = new Ressource_Img(setup->getRenderer(), "img/spite.png");

	SDL_SetRenderDrawColor(setup->getRenderer(), 192, 192, 192, 255);

	tabWidth = passed_tabWidth;
	tabHeight = passed_tabHeight;
	tab = new Case[tabWidth*tabHeight];
	//for (int i = 0; i < tabWidth*tabHeight; ++i)
		//tab[i] = Case();

	print = new VisibleEntity(setup->getRenderer(), spite->getTexture(), CASE_SIZE, CASE_SIZE, Y_CASE);
	count = new VisibleEntity(setup->getRenderer(), spite->getTexture(), COUNT_W, COUNT_H, Y_COUNT);
	count->setPos(CASE_SIZE, CASE_SIZE);
	smiley = new VisibleEntity(setup->getRenderer(), spite->getTexture(), SMILEY_SIZE, SMILEY_SIZE, Y_SMILEY);
	smiley->setPos((((passed_tabWidth + 2)*CASE_SIZE) / 2) - (SMILEY_SIZE / 2), CASE_SIZE);

	srand(time(NULL));
}

GameScreen::~GameScreen()
{
	delete print;
	delete count;
	delete smiley;
	delete spite;
	delete[] tab;
	delete setup;
}

void GameScreen::LoopScreen() {

	do {
		Reset();
		Generate();
		
		repeat = false;
		bool running = true;
		bool clickLeft = false, clickRight = false;
		unsigned int lastTime = 0, preTime = 0;
		int timer = 0;
		SDL_TimerID sdl_timer = SDL_AddTimer(2000, incTimer, &timer);

		while (running) {

			LoadScreen(timer);
			
			lastTime = SDL_GetTicks();
			if (lastTime - preTime < 30) {
				SDL_Delay(30 - (lastTime - preTime));
			}
			preTime = lastTime;
			
			SDL_FlushEvent(SDL_MOUSEMOTION);
			//SDL_WaitEvent(setup->getMainEvent());

			SDL_PollEvent(setup->getMainEvent());

			switch (setup->getMainEvent()->type)
			{
			case SDL_MOUSEBUTTONDOWN:
				if (setup->getMainEvent()->button.button == SDL_BUTTON_LEFT) {
					clickLeft = true;
				}
				else if (setup->getMainEvent()->button.button == SDL_BUTTON_RIGHT && !clickRight) {
					Flaging(setup->getMainEvent()->button.x, setup->getMainEvent()->button.y);
					clickRight = true;
					std::cout << "clickRight " << clickRight << std::endl;
					//SDL_FlushEvent(SDL_MOUSEBUTTONDOWN);
				}
				break;
			case SDL_MOUSEBUTTONUP:
				smiley->setRect(BORD, Y_SMILEY);
				switch (setup->getMainEvent()->button.button)
				{
				case SDL_BUTTON_LEFT:
					clickLeft = false;
					running = Play(setup->getMainEvent()->button.x, setup->getMainEvent()->button.y);
					break;
				case SDL_BUTTON_RIGHT:
					if (clickRight) {
						clickRight = false;
						std::cout << "clickRight" << clickRight << std::endl;
						//SDL_FlushEvent(SDL_MOUSEBUTTONUP);
					}
					break;
				}
				break;
			case SDL_QUIT:
				running = false;
				quit = true;
				break;
			case SDL_KEYDOWN:
				switch (setup->getMainEvent()->key.keysym.sym)
				{
				case SDLK_BACKSPACE:
					repeat = true;
				case SDLK_ESCAPE:
					running = false;
					quit = true;
					break;
				case SDLK_m:
					setup->MinimizeWindow();
					break;
				}
				break;
			}
			
			if (clickLeft)
				Select(setup->getMainEvent()->button.x, setup->getMainEvent()->button.y);

			if (running)
				running = !(Win() || timer >= 999);
		}
		SDL_RemoveTimer(sdl_timer);

		if (Win())
			smiley->setRect(BORD + (SMILEY_SIZE + 1) * 3, Y_SMILEY);
		else if (!smiley->ClickedOn(setup->getMainEvent()->button.x, setup->getMainEvent()->button.y))
			smiley->setRect(BORD + (SMILEY_SIZE + 1) * 4, Y_SMILEY);

		LoadScreen(timer);
		running = true;
		
		while (running && !repeat && !quit) {

			SDL_WaitEvent(setup->getMainEvent());

			switch (setup->getMainEvent()->type)
			{
			case SDL_QUIT:
				running = false;
				quit = true;
				break;
			case SDL_KEYDOWN:
				switch (setup->getMainEvent()->key.keysym.sym)
				{
				case SDLK_BACKSPACE:
					repeat = true;
					running = false;
					break;
				case SDLK_ESCAPE:
					repeat = false;
					running = false;
					quit = true;
					break;
				}
				break;
			case SDL_MOUSEBUTTONUP:
				if (setup->getMainEvent()->button.button == SDL_BUTTON_LEFT &&
					smiley->ClickedOn(setup->getMainEvent()->button.x, setup->getMainEvent()->button.y))
					repeat = true;
				break;
			}
		}

	} while (repeat && !quit);
}

void GameScreen::LoadScreen(int timer) {
	setup->Begin();

	for (int i = 0; i < tabWidth; ++i) {
		for (int j = 0; j < tabHeight; ++j) {
			if (getCase(i, j).isHidden()) {
				if(getCase(i,j).isFlaged())
					print->setRect(BORD + (CASE_SIZE + 1) * 2, Y_CASE);
				else if (getCase(i,j).isSelected())
					print->setRect(BORD + (CASE_SIZE + 1) * 1, Y_CASE);
				else
					print->setRect(BORD, Y_CASE);
			}
			else {
				if (getCase(i, j).isBomb()) {
					if (getCase(i, j).isFlaged())
						print->setRect(BORD + (CASE_SIZE + 1) * 7, Y_CASE);
					else if (getCase(i,j).isSelected())
						print->setRect(BORD + (CASE_SIZE + 1) * 6, Y_CASE);
					else
						print->setRect(BORD + (CASE_SIZE + 1) * 5, Y_CASE);
				}
				else {
					if (getCase(i,j).getNumber() == 0)
						print->setRect(BORD + (CASE_SIZE + 1) * 1, Y_CASE);
					else
						print->setRect(BORD + (CASE_SIZE + 1) * (getCase(i, j).getNumber()-1), Y_NUMBER);
				}
			}
			print->setPos((i + 1)*CASE_SIZE, (j + 2)* CASE_SIZE + COUNT_H);
			print->Draw();
		}
	}
	
	count->DrawCount(tabWidth, timer, 3);

	count->DrawCount(3, nbBomb, 3);

	smiley->Draw();

	setup->End();
}

int GameScreen::getMouseX(int x) {
	return (int)floor(x / (CASE_SIZE*PRP))-1;
}

int GameScreen::getMouseY(int y) {
	return (int)floor((y-(7*PRP))/ (CASE_SIZE*PRP))-3;
}

bool GameScreen::Play(int x, int y) {
	if (checkClickOnTab(x, y))
		return clickOnTab(getMouseX(x), getMouseY(y));
	else if (smiley->ClickedOn(x, y)) {
		repeat = true;
		return false;
	}
	else
		return true;
}

bool GameScreen::clickOnTab(int x, int y) {
	
	if (x >= 0 && y >= 0 && getCase(x, y).isHidden() && !getCase(x, y).isFlaged()) {
		Discovered(x, y);
		if (getCase(x, y).isBomb()) {
			tab[x*(tabHeight)+y].Select();
			for (int i = 0; i < tabWidth; ++i)
				for (int j = 0; j < tabHeight; ++j)
					if (getCase(i, j).isBomb())
						Discovered(i, j);
			return false;
		}
		else if (getCase(x, y).getNumber() == 0) {
			searchEmpty(x, y);
		}
	}
	return true;
}

void GameScreen::searchEmpty(int x, int y) {

	for (int i = x - 1;  i <= x+1; ++i) {
		for (int j = y - 1;  j <= y+1; ++j) {
			if (i >= 0 && i < tabWidth && j >= 0 && j < tabHeight && 
				getCase(i, j).isHidden()) {
				Discovered(i, j);
				if (getCase(i, j).getNumber() == 0)
					searchEmpty(i, j);
			}
		}
	}
}

Case GameScreen::getCase(int x, int y) {
	assert(x >= 0 && x < tabWidth && y >= 0 && y < tabHeight);
	return tab[x*(tabHeight) + y];
}

void GameScreen::Discovered(int x, int y) {
	tab[x*(tabHeight)+y].setDiscovered();
}

void GameScreen::Flaging(int x, int y) {
	if (checkClickOnTab(x, y)) {
		x = getMouseX(x);
		y = getMouseY(y);
		if (getCase(x, y).isHidden()) {
			if (getCase(x, y).isFlaged()) {
				tab[x*(tabHeight)+y].unsetFlag();
				nbBomb++;
			}
			else {
				tab[x*(tabHeight)+y].setFlag();
				if (nbBomb > 0)
					nbBomb--;
			}
		}
	}
}

void GameScreen::Select(int x, int y) {
	if (checkClickOnTab(x, y)) {
		UnselectAll();
		tab[getMouseX(x) * tabHeight + getMouseY(y)].Select();
		smiley->setRect(BORD + (SMILEY_SIZE + 1) * 2, Y_SMILEY);
	}
	else if (smiley->ClickedOn(x, y)) {
		smiley->setRect(BORD + (SMILEY_SIZE + 1) * 1, Y_SMILEY);
	}

}

void GameScreen::UnselectAll() {
	for (int i = 0; i < tabWidth*tabHeight; ++i) {
		tab[i].Unselect();
	}
}

void GameScreen::Generate() {
	nbBomb = (int)rint(tabWidth*tabHeight*0.15);
	if (nbBomb % 2 != 0)
		nbBomb++;
	int x, y;
	for (int i = 0; i < nbBomb; ++i) {
		do{
			x = rand() % (tabWidth - 1);
			y = rand() % (tabHeight - 1);
		}while (getCase(x, y).isBomb());

		tab[x*(tabHeight)+y].setBomb(true);
		for (int j = x - 1; j <= x + 1; ++j)
			for (int k = y - 1; k <= y + 1; ++k)
				if (j >= 0 && k >= 0 && j < tabWidth && k < tabHeight)
					tab[j*(tabHeight)+k].incNumber();
	}
}

void GameScreen::Reset() {
	for (int i = 0; i < tabWidth*tabHeight; ++i)
		tab[i].reset();
	smiley->setRect(BORD, Y_SMILEY);
}

bool GameScreen::Win() {
	for (int i = 0; i < tabWidth*tabHeight; ++i)
		if (tab[i].isHidden() && !tab[i].isBomb())
			return false;

	return true;
}

bool GameScreen::checkClickOnTab(int x, int y) {
	return (y > PRP*(2 * CASE_SIZE + SMILEY_SIZE) &&
		y < setup->getScreenHeight() - (CASE_SIZE+3)*PRP &&
		x > CASE_SIZE*PRP && x < setup->getScreenWidth() - CASE_SIZE*PRP);
}

Uint32 incTimer(Uint32 interval, void* param) {
	int* timer = (int*)param;
	*timer = *timer + 1;

	/*
	SDL_Event event;
	SDL_UserEvent userevent;

	

	userevent.type = SDL_USEREVENT;
	userevent.code = 0;
	userevent.data1 = NULL;
	userevent.data2 = NULL;

	event.type = SDL_USEREVENT;
	event.user = userevent;

	SDL_PushEvent(&event);
	*/
	return interval;
}