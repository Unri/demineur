#pragma once
#ifndef GAMESCREEN_H
#define GAMESCREEN_H


#include <SDL\SDL.h>
#include <SDL\SDL_image.h>
#include <vector>

#include "const.h"
#include "Case.h"
#include "Ressource_Img.h"
#include "VisibleEntity.h"

class GameScreen
{
public:
	GameScreen(std::string windowName, int passed_tabWidth, int passed_tabHeight);
	virtual ~GameScreen();

	void LoopScreen();
	void LoadScreen(int timer);

private:
	CSDL_Setup* setup;

	Ressource_Img* spite;
	bool quit;
	bool repeat;

	Case* tab;

	int tabWidth;
	int tabHeight;
	int nbBomb;


	VisibleEntity* print;
	VisibleEntity* count;
	VisibleEntity* smiley;

	void Generate();
	void Reset();
	bool Play(int x, int y);
	bool Win();

	int getMouseX(int x);
	int getMouseY(int y);

	Case getCase(int x, int y);
	void Discovered(int x, int y);
	void Flaging(int x, int y);
	void Select(int x, int y);
	void searchEmpty(int x, int y);
	bool clickOnTab(int x, int y);
	bool checkClickOnTab(int x, int y);
	void UnselectAll();
};

Uint32 incTimer(Uint32 interval, void* param);

#endif // GAMESCREEN_H