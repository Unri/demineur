#include "stdafx.h"
#include "Ressource_Img.h"


Ressource_Img::Ressource_Img(SDL_Renderer* passed_renderer, std::string passed_filePath)
{
	renderer = passed_renderer;
	filePath = passed_filePath;
	texture = NULL;
	texture = IMG_LoadTexture(renderer, filePath.c_str());
	if (texture == NULL) // Si l'ouverture a �chou�, on le note et on arr�te
	{
		std::cerr << "Impossible de charger la texture: " << filePath << SDL_GetError() << std::endl;
		exit(EXIT_FAILURE);
	}
}


Ressource_Img::~Ressource_Img()
{
	SDL_DestroyTexture(texture);
}

SDL_Texture* Ressource_Img::getTexture() const{
	return texture;
}
