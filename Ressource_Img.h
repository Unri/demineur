#pragma once
#ifndef RESSOURCE_IMG_H
#define RESSOURCE_IMG_H


#include <string>
#include <iostream>
#include <SDL.h>
#include <SDL\SDL_image.h>

class Ressource_Img
{
public:
	Ressource_Img(SDL_Renderer* passed_renderer, std::string passed_filePath);
	virtual ~Ressource_Img();

	SDL_Texture* getTexture() const;

private:
	std::string filePath;
	SDL_Texture* texture;
	SDL_Renderer* renderer;
};

#endif // !RESSOURCE_IMG_H